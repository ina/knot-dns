Source: knot
Section: net
Priority: optional
Maintainer: knot packagers <knot@packages.debian.org>
Uploaders:
 Ondřej Surý <ondrej@debian.org>,
 Daniel Salzman <daniel.salzman@nic.cz>,
 Daniel Kahn Gillmor <dkg@fifthhorseman.net>,
 Robert Edmonds <edmonds@debian.org>,
 Jakub Ružička <jakub.ruzicka@nic.cz>,
Build-Depends-Indep:
 python3-setuptools,
 python3-sphinx,
 texinfo,
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 libbpf-dev [!armel !kfreebsd-i386 !kfreebsd-amd64 !hurd-i386],
 libcap-ng-dev [!kfreebsd-i386 !kfreebsd-amd64 !hurd-i386],
 libedit-dev,
 libelf-dev [!armel !kfreebsd-i386 !kfreebsd-amd64 !hurd-i386],
 libfstrm-dev,
 libgnutls28-dev,
 libidn2-dev,
 liblmdb-dev,
 libmaxminddb-dev,
 libnghttp2-dev,
 libprotobuf-c-dev,
 libsofthsm2 <!nocheck>,
 libsystemd-dev [linux-any] | libsystemd-daemon-dev [linux-any],
 libsystemd-dev [linux-any] | libsystemd-journal-dev [linux-any],
 liburcu-dev (>= 0.4),
 linux-libc-dev [!kfreebsd-i386 !kfreebsd-amd64 !hurd-i386],
 pkg-config,
 protobuf-c-compiler,
 python3-all,
 python3-yaml (>= 5.1) <!nocheck>,
Standards-Version: 4.5.1
Homepage: https://www.knot-dns.cz/
Vcs-Browser: https://salsa.debian.org/dns-team/knot-dns
Vcs-Git: https://salsa.debian.org/dns-team/knot-dns.git
Rules-Requires-Root: no

Package: knot
Architecture: any
Depends:
 adduser,
 libdnssec8 (= ${binary:Version}),
 libknot11 (= ${binary:Version}),
 libzscanner3 (= ${binary:Version}),
 lsb-base (>= 3.0-6),
 python3,
 python3-yaml,
 ${misc:Depends},
 ${shlibs:Depends},
Pre-Depends:
 ${misc:Pre-Depends},
Breaks:
 knot-dnsutils (<< 2.9.2-5),
Recommends:
 python3-lmdb,
Suggests:
 systemd,
Description: Authoritative domain name server
 Knot DNS is a fast, authoritative only, high performance, feature
 full and open source name server.
 .
 Knot DNS is developed by CZ.NIC Labs, the R&D department of .CZ
 registry and hence is well suited to run anything from the root
 zone, the top-level domain, to many smaller standard domain names.

Package: libknot11
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Section: libs
Description: Authoritative domain name server (shared library)
 Knot DNS is a fast, authoritative only, high performance, feature
 full and open source name server.
 .
 Knot DNS is developed by CZ.NIC Labs, the R&D department of .CZ
 registry and hence is well suited to run anything from the root
 zone, the top-level domain, to many smaller standard domain names.
 .
 This package provides libknot shared library used by Knot DNS and
 Knot Resolver.

Package: libzscanner3
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Section: libs
Description: DNS zone-parsing library from Knot
 Knot DNS is a fast, authoritative only, high performance, feature
 full and open source name server.
 .
 Knot DNS is developed by CZ.NIC Labs, the R&D department of .CZ
 registry and hence is well suited to run anything from the root
 zone, the top-level domain, to many smaller standard domain names.
 .
 This package provides a fast zone parser shared library used by Knot
 DNS and Knot Resolver.

Package: libdnssec8
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Section: libs
Description: DNSSEC shared library from Knot
 Knot DNS is a fast, authoritative only, high performance, feature
 full and open source name server.
 .
 Knot DNS is developed by CZ.NIC Labs, the R&D department of .CZ
 registry and hence is well suited to run anything from the root
 zone, the top-level domain, to many smaller standard domain names.
 .
 This package provides common DNSSEC shared library used by Knot DNS
 and Knot Resolver.

Package: libknot-dev
Architecture: any
Multi-Arch: same
Depends:
 libdnssec8 (= ${binary:Version}),
 libgnutls28-dev,
 libknot11 (= ${binary:Version}),
 libzscanner3 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Section: libdevel
Description: Knot DNS shared library development files
 Knot DNS is a fast, authoritative only, high performance, feature
 full and open source name server.
 .
 Knot DNS is developed by CZ.NIC Labs, the R&D department of .CZ
 registry and hence is well suited to run anything from the root
 zone, the top-level domain, to many smaller standard domain names.
 .
 This package provides development files for internal common shared
 libraries.

Package: knot-dnsutils
Architecture: any
Depends:
 libdnssec8 (= ${binary:Version}),
 libknot11 (= ${binary:Version}),
 libzscanner3 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Breaks:
 knot (<< 2.9.2-5),
Replaces:
 knot (<< 2.9.2-5),
Description: Clients provided with Knot DNS (kdig, knsupdate, kzonecheck)
 Knot DNS is a fast, authoritative only, high performance, feature
 full and open source name server.
 .
 Knot DNS is developed by CZ.NIC Labs, the R&D department of .CZ
 registry and hence is well suited to run anything from the root
 zone, the top-level domain, to many smaller standard domain names.
 .
 This package delivers various client programs related to DNS that are
 derived from the Knot DNS source tree.
 .
  - kdig - query a DNS server in various ways
  - knsupdate - perform dynamic updates (See RFC2136)
  - kxdpgun - send a DNS query stream over UDP to a DNS server
  - kzonecheck - zone check tool
 .
 Those clients were designed to be 1:1 compatible with BIND dnsutils,
 but they provide some enhancements, which are documented in respective
 manpages.
 .
 WARNING: knslookup is not provided as it is considered obsolete.

Package: knot-host
Architecture: any
Depends:
 libdnssec8 (= ${binary:Version}),
 libknot11 (= ${binary:Version}),
 libzscanner3 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Version of 'host' bundled with Knot DNS
 Knot DNS is a fast, authoritative only, high performance, feature
 full and open source name server.
 .
 Knot DNS is developed by CZ.NIC Labs, the R&D department of .CZ
 registry and hence is well suited to run anything from the root
 zone, the top-level domain, to many smaller standard domain names.
 .
 This package provides the 'host' program in the form that is bundled
 with the Knot DNS.  The 'host' command is designed to be 1:1
 compatible with BIND 9.x 'host' program.

Package: knot-module-dnstap
Architecture: any
Depends:
 knot (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: dnstap module for Knot DNS
 Knot DNS is a fast, authoritative only, high performance, feature
 full and open source name server.
 .
 Knot DNS is developed by CZ.NIC Labs, the R&D department of .CZ
 registry and hence is well suited to run anything from the root
 zone, the top-level domain, to many smaller standard domain names.
 .
 This package contains dnstap module for logging DNS traffic.

Package: knot-module-geoip
Architecture: any
Depends:
 knot (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: geoip module for Knot DNS
 Knot DNS is a fast, authoritative only, high performance, feature
 full and open source name server.
 .
 Knot DNS is developed by CZ.NIC Labs, the R&D department of .CZ
 registry and hence is well suited to run anything from the root
 zone, the top-level domain, to many smaller standard domain names.
 .
 This package contains geoip module for geography-based responses.

Package: knot-doc
Architecture: all
Multi-Arch: foreign
Depends:
 libjs-jquery,
 libjs-underscore,
 ${misc:Depends},
Section: doc
Description: Documentation for Knot DNS
 Knot DNS is a fast, authoritative only, high performance, feature
 full and open source name server.
 .
 Knot DNS is developed by CZ.NIC Labs, the R&D department of .CZ
 registry and hence is well suited to run anything from the root
 zone, the top-level domain, to many smaller standard domain names.
 .
 This package provides various documents that are useful for
 maintaining a working Knot DNS installation.

Package: python3-libknot
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Section: python
Description: Python bindings for libknot
 Knot DNS is a fast, authoritative only, high performance, feature
 full and open source name server.
 .
 Knot DNS is developed by CZ.NIC Labs, the R&D department of .CZ
 registry and hence is well suited to run anything from the root
 zone, the top-level domain, to many smaller standard domain names.
 .
 This package provides Python bindings for the libknot shared library.
